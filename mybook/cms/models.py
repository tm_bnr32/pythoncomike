# Create your models here.
from django.db import models


class Book(models.Model):
    """売上"""
    comike = models.CharField('コミックマーケット', max_length=255)
    title = models.CharField('同人誌タイトル', max_length=255, blank=True)
    price = models.IntegerField('単価', blank=True, default=0)
    cost = models.IntegerField('原価', blank=True, default=0)
    stock = models.IntegerField('冊数', blank=True, default=0)
    sell = models.IntegerField('売上冊数', blank=True, default=0)

    def __str__(self):
        return self.comike


class Impression(models.Model):
    """感想"""
    book = models.ForeignKey(Book, verbose_name='書籍', related_name='impressions')
    comment = models.TextField('コメント', blank=True)

    def __str__(self):
        return self.comment