from django.forms import ModelForm
from cms.models import Book, Impression

class BookForm(ModelForm):
    """書籍のフォーム"""
    class Meta:
        model = Book
        fields = ('comike', 'title','price', 'cost', 'stock', 'sell', 'stock', )


class ImpressionForm(ModelForm):
    """感想のフォーム"""
    class Meta:
        model = Impression
        fields = ('comment', )